#terraform {
#  backend "local" {}
#}
terraform {
  backend "s3" {
    bucket = "terraform-backend.erosson.org"
    key    = "terraform-module-test.erosson.org"
    region = "us-east-1"
  }
}

locals {
  subdomain   = "terraform-module-test"
  hostdomain  = "erosson.org"
  fulldomain  = "${local.subdomain}.${local.hostdomain}"
  gitlab_user = "erosson"

  # Encrypt the deploy user's secret key in terraform state.
  # Use any keybase user here; the secret key's not needed after provisioning.
  pgp_key = "keybase:erosson"
}

# Given the above local variables, the code below creates a static website
# on a subdomain of erosson.org, with partial continous deployment configuration.
#
# More specifically, it creates:
# * A gitlab repository
# * An S3 bucket for static web hosting
# * A Cloudflare DNS subdomain record aimed at the S3 bucket
# * An AWS user for deploying to the S3 bucket, with minimum permissions
# * Gitlab or Travis CI auth configuration for the above AWS user, and for
#   Cloudflare (for cache clearing on deployment). CI configuration to actually
#   build and deploy is not done here; this only makes auth variables available.
#
# "Why can't all the boilerplate below go into its own module?"
# I think that'd be abstracting away too much. Many projects need to tweak
# *something* below here, and I'd rather give them some lower-level config that
# allows changes, than a single "static_website" module that's surely missing
# necessary flags. Maybe I'll revisit that approach after I use this on a few
# more projects, and see what configuration needs are most common - but not yet.
#
# TODO: cloudfront configuration for redirecting everything to index.html.
# TODO: route53 vs cloudflare? with cloud*front*, cloud*flare* may be redundant.

provider "aws" {
  version = "~> 1.47"
  region  = "us-east-1"
}

provider "cloudflare" {
  version = "~> 1.2"
}

provider "gitlab" {
  version = "~> 1.0"
}

resource "gitlab_project" "git" {
  name             = "${local.subdomain}-erosson-org"
  description      = "https://${local.fulldomain} - erosson/terraform module demo"
  visibility_level = "public"
}

module "user" {
  source  = "../../aws_static_www/user"
  bucket  = "${local.fulldomain}"
  pgp_key = "${local.pgp_key}"
}

resource "aws_s3_bucket" "web" {
  bucket = "${module.user.bucket}"
  acl    = "public-read"
  policy = "${module.user.bucket_policy}"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

module "cloudfront" {
  source = "../../aws_static_www/cloudfront"
  domain = "${local.fulldomain}"
  origin = "${aws_s3_bucket.web.bucket_regional_domain_name}"
}

resource "cloudflare_record" "dns" {
  domain  = "${local.hostdomain}"
  name    = "${local.subdomain}"
  type    = "CNAME"
  value   = "${aws_s3_bucket.web.website_domain}"
  proxied = true
}

module "static_www_travis" {
  source           = "../../aws_static_www/travis"
  dry_run          = "1"
  id               = "${module.user.id}"
  encrypted_secret = "${module.user.encrypted_secret}"
}

module "static_www_gitlab_ci" {
  source = "../../aws_static_www/gitlab_ci"

  #dry_run          = "1"
  repository       = "${local.gitlab_user}/${gitlab_project.git.name}"
  id               = "${module.user.id}"
  encrypted_secret = "${module.user.encrypted_secret}"
}
