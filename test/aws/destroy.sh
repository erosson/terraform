#!/bin/sh -eux
aws s3 ls s3://terraform-module-test.erosson.org
aws s3 rm --recursive s3://terraform-module-test.erosson.org/
aws s3 ls s3://terraform-module-test.erosson.org
