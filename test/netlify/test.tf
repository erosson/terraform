terraform {
  backend "s3" {
    bucket = "terraform-backend.erosson.org"
    key    = "terraform-module-test-netlify.erosson.org"
    region = "us-east-1"
  }
}

locals {
  project         = "terraform-module-test-netlify"
  hostdomain      = "erosson.org"
  fulldomain      = "${local.project}.${local.hostdomain}"
  gitlab_user     = "erosson"
  gitlab_path     = "${local.gitlab_user}/${gitlab_project.git.name}"
  netlify_project = "${local.project}-erosson-org"
}

provider "cloudflare" {
  version = "~> 1.2"
}

provider "gitlab" {
  version = "~> 1.0"
}

provider "netlify" {
  version = "~> 0.1"
}

resource "gitlab_project" "git" {
  name             = "${local.project}"
  description      = "https://${local.fulldomain}"
  visibility_level = "public"
  default_branch   = "master"

  provisioner "local-exec" {
    command = <<EOF
sh -eu
git remote remove netlify-test-repo || true
git remote add netlify-test-repo ${gitlab_project.git.ssh_url_to_repo}
git push netlify-test-repo master
EOF
  }
}

module "netlify_site" {
  source        = "../../netlify/gitlab"
  name          = "${local.netlify_project}"
  custom_domain = "${local.fulldomain}"

  repo {
    repo_branch = "master"
    command     = "cp -rp test/netlify/www build"
    dir         = "build"
    repo_path   = "${local.gitlab_path}"
  }
}

resource "cloudflare_record" "dns" {
  domain  = "${local.hostdomain}"
  name    = "${local.project}"
  type    = "CNAME"
  value   = "${module.netlify_site.dns}"
  proxied = false
}
