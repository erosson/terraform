variable "origin" {
  type = "string"
}

variable "domain" {
  type = "string"
}

variable "enabled" {
  default = true
}
