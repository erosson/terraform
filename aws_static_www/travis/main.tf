provider "null" {
  version = "~> 1.0"
}

locals {
  run_dry    = "echo "
  run_wet    = ""
  run        = "${var.dry_run != "" ? local.run_dry : local.run_wet }"
  trigger_id = "${var.dry_run != "" ? uuid() : var.trigger_id }"       # dry-run always triggers a rerun
}

resource "null_resource" "travis" {
  triggers = {
    id = "${local.trigger_id}"
  }

  provisioner "local-exec" {
    command = <<EOF
set -eu
${local.run}travis env set --public AWS_ACCESS_KEY_ID ${var.id}
${local.run}travis env set --private AWS_SECRET_ACCESS_KEY $(echo ${var.encrypted_secret} | base64 --decode | keybase pgp decrypt)
${local.run}travis env set --private CLOUDFLARE_TOKEN $CLOUDFLARE_TOKEN
${local.run}travis env set --public CLOUDFLARE_EMAIL $CLOUDFLARE_EMAIL
${local.run}travis env list
EOF
  }
}
