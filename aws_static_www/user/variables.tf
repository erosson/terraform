variable "bucket" {
  type="string"
}
variable "user" {
  type="string"
  default=""
}
variable "pgp_key" {}
