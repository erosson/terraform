output "user" {
  value = "${local.user}"
}

output "id" {
  value = "${aws_iam_access_key.deploy.id}"
}

output "encrypted_secret" {
  value = "${aws_iam_access_key.deploy.encrypted_secret}"
}

output "bucket_policy" {
  value = "${module.bucket.bucket_policy}"
}

output "bucket" {
  value = "${var.bucket}"
}
