module "bucket" {
  source = "../bucket"
  bucket = "${var.bucket}"
}

locals {
  default_user = "deploy@${var.bucket}"
  user         = "${var.user != "" ? var.user : local.default_user }"
}

# A newly-created AWS user deploys to S3 on commit.
# Permissions are restricted to this project's S3 bucket.
resource "aws_iam_user" "deploy" {
  name = "${local.user}"
}

# https://www.terraform.io/docs/providers/aws/r/iam_access_key.html
# Secret key is written to terraform's state file, encrypted with my secret key.
#
# from https://www.terraform.io/docs/providers/aws/r/iam_access_key.html :
# > terraform output deploy_access_key
# > terraform output deploy_secret_key | base64 --decode | keybase pgp decrypt
resource "aws_iam_access_key" "deploy" {
  user    = "${aws_iam_user.deploy.name}"
  pgp_key = "${var.pgp_key}"
}

resource "aws_iam_user_policy" "deploy" {
  name = "${aws_iam_user.deploy.name}"
  user = "${aws_iam_user.deploy.name}"

  # minimum-access s3 policy for the deploying user.
  # this helped, though CI might not use s3sync:
  # https://blog.willj.net/2014/04/18/aws-iam-policy-for-allowing-s3cmd-to-sync-to-an-s3-bucket/
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${var.bucket}",
                "arn:aws:s3:::${var.bucket}/*"
            ]
        }
    ]
}
EOF
}
