variable "trigger_id" {
  type    = "string"
  default = ""
}

variable "dry_run" {
  type    = "string"
  default = ""
}

variable "repository" {
  type = "string"
}

variable "id" {
  type = "string"
}

variable "encrypted_secret" {
  type = "string"
}
