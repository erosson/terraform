provider "null" {
  version = "~> 1.0"
}

locals {
  run_dry    = "echo "
  run_wet    = ""
  run        = "${var.dry_run != "" ? local.run_dry : local.run_wet }"
  trigger_id = "${var.dry_run != "" ? uuid() : var.trigger_id }"       # dry-run always triggers a rerun
  repository = "${replace("${var.repository}", "/", "%2F")}"
}

resource "null_resource" "gitlab_ci" {
  triggers = {
    id = "${local.trigger_id}"
  }

  # set CI variables. There is no create-or-update: PUT updates, POST creates.
  # https://docs.gitlab.com/ee/api/project_level_variables.html
  provisioner "local-exec" {
    command = <<EOF
set -eu
gitlab_curl() {
  local API_PATH=$1;shift
  # -f is important: return a non-zero exit code for http status 4xx.
  ${local.run}curl -f --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/${local.repository}$API_PATH" "$@"
}
gitlab_var() {
  # create or update a gitlab CI variable.
  local KEY=$1;shift
  local VAL=$1;shift
  # first, try to update. if error, it might not exist - try to create. the api doesn't give us one create-or-update function.
  # ideally these would be terraform resources, but this is good enough for a first pass.
  gitlab_curl "/variables/$KEY" --request PUT --form "value=$VAL" \
  || gitlab_curl "/variables" --request POST --form "key=$KEY" --form "value=$VAL"
}
gitlab_var AWS_ACCESS_KEY_ID "${var.id}"
gitlab_var AWS_SECRET_ACCESS_KEY $(echo ${var.encrypted_secret} | base64 --decode | keybase pgp decrypt)
gitlab_var CLOUDFLARE_EMAIL "$CLOUDFLARE_EMAIL"
gitlab_var CLOUDFLARE_TOKEN "$CLOUDFLARE_TOKEN"
gitlab_curl "/variables"
EOF
  }
}
