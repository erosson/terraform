output "name" {
  value = var.name
}

output "dns" {
  value = "${var.name}.netlify.com"
}
