resource "gitlab_deploy_key" "key" {
  # I'm required for netlify to trigger a build on git push
  #project  = "${gitlab_project.git.id}"
  project = var.repo["repo_path"]

  title    = "netlify deploy key"
  key      = netlify_deploy_key.key.public_key
  can_push = false
}

# https://github.com/terraform-providers/terraform-provider-netlify
resource "gitlab_project_hook" "on_push" {
  project                 = var.repo["repo_path"]
  url                     = netlify_build_hook.on_push.url
  enable_ssl_verification = true
  push_events             = true
  merge_requests_events   = true
}

resource "netlify_deploy_key" "key" {}

resource "netlify_site" "main" {
  # because S3 can't do pushState urls without painful cloudfront config
  name          = var.name
  custom_domain = var.custom_domain

  #repo          = ["${merge(var.repo, map("deploy_key_id", "${netlify_deploy_key.key.id}"))}"]
  repo {
    repo_branch   = var.repo["repo_branch"]
    command       = var.repo["command"]
    deploy_key_id = netlify_deploy_key.key.id
    dir           = var.repo["dir"]
    provider      = "gitlab"
    repo_path     = var.repo["repo_path"]
  }
}

resource "netlify_build_hook" "on_push" {
  site_id = netlify_site.main.id
  branch  = "master"
  title   = "build on push"
}

#resource "netlify_hook" "on_deploy_success" {
#  site_id = "${netlify_site.main.id}"
#  type    = "gitlab"
#  event   = "deploy_created"
#  data = {}
#}
#
#resource "netlify_hook" "on_deploy_start" {
#  site_id = "${netlify_site.main.id}"
#  type    = "gitlab"
#  event   = "deploy_building"
#  data    = {}
#}
#
#resource "netlify_hook" "on_deploy_fail" {
#  site_id = "${netlify_site.main.id}"
#  type    = "gitlab"
#  event   = "deploy_failed"
#  data    = {}
#}
