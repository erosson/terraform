variable "name" {
  type = string
}

variable "custom_domain" {
  type = string
}

variable "repo" {
  type = map
}
