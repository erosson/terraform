# terraform

my custom terraform modules

https://www.terraform.io/docs/modules/sources.html

usage:

  module "erosson" {
    source = "git::https://gitlab.com/erosson/terraform.git"
    # or
    # source = "git::https://gitlab.com/erosson/terraform.git?ref=somegithash"
  }
